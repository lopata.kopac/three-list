import DestopTaps from './destop-taps';
import DestopTap from './destop-taps';
import FileTaps from './file-taps';
import FileContent from './file-content';

export { DestopTaps, DestopTap, FileTaps, FileContent };