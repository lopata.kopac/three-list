import React, { useContext } from 'react';
import AplikacionContext from '../context/aplikacion';
import { Card } from 'antd';
import './history.css';

const historyMap = (history) => history.map(item => <Card className='history-item'>{item.name}</Card>);

function History(){
    const {history} = useContext(AplikacionContext);

    return(
        <Card className='history' title="History">
            {historyMap(history)}
        </Card>
    );
}

export default History;