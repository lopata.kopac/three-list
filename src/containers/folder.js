import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { Collapse, Row, Col, Card } from 'antd';
import File from './file';
import './folder.css';

const { Panel } = Collapse;

const dataGql = gql`{
    getList{
        id
        name
        type
    }
}`;

const dataById = id => gql`{
    getList(id: "${id}"){
        id
        name
        type
    }
}`;

const foldersMap = data => data.map(item => { return <Panel  header={item.name}><Folder id={item.id}/></Panel>;});

const filesMap = data => data.map(item => { return <Col className='file-item-card' span={6}><File className='file-card' {...item}/></Col> });

const getFolders = data => data.getList.filter((item) => item.type === 'FOLDER');

const getFiles = data => data.getList.filter((item) => item.type === 'FILE');

function Folder({ id }){
    const { loading, error, data } = useQuery(id ? dataById(id) : dataGql);

    if(loading) return (<div>loading</div>)

    if(error) return <div>Error :(</div>

    return(
        <Collapse className='folder-panel'>
            <Row>
                <Card className='files-list-body'>{filesMap(getFiles(data))}</Card>
            </Row>
            {foldersMap(getFolders(data))}
        </Collapse>
    );
}

export default Folder;