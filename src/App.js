import React from 'react';
import { ConfigProvider } from 'antd';
import './App.css';
import { DestopTaps } from './compoments/index';
import { AplikacionProvider } from './context/aplikacion';
import { ApolloProvider } from '@apollo/react-hooks';
import ApolloClient from 'apollo-boost';
import { AppContent } from './containers/index';

const client = new ApolloClient({
  uri: 'https://react-test.atlasconsulting.cz/',
});

function App() {
  return (
    <ConfigProvider>
      <DestopTaps >
        <AplikacionProvider>
          <ApolloProvider client={client}>
            <AppContent />
          </ApolloProvider>
        </AplikacionProvider>
      </DestopTaps>
    </ConfigProvider>
  );
}

export default App;
