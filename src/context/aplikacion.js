import React, {useState} from 'react';

const AplikacionContext = React.createContext();

function AplikacionProvider({children}){
    const [appStatus, setAppStatus] = useState({
        history:[],
        openedFiles:[],
    });

    const openFileHandle = (file) =>{
        setAppStatus({...appStatus, 
            history: appStatus.history.concat(file),
            openedFiles: appStatus.openedFiles.concat(file)
        });
    }

    const closeFilehandle = (id) =>{
        setAppStatus({...appStatus, openedFiles: appStatus.openedFiles.filter(item => item.id !== id)});
    }

    return(
        <AplikacionContext.Provider 
        value={{
            ...appStatus,
            openFile: openFileHandle,
            closeFile: closeFilehandle
        }}
        >
            {children}
        </AplikacionContext.Provider>
    );
}

export default AplikacionContext;
export { AplikacionProvider };